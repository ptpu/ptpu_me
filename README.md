# ptpu.me

[![status-badge](https://ci.codeberg.org/api/badges/ptpu/ptpu_me/status.svg)](https://ci.codeberg.org/ptpu/ptpu_me)

Project data for the [Hugo](http://gohugo.io/) generated static website [ptpu.me](https://ptpu.me) using [Codeberg Pages](https://codeberg.page/).