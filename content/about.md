---
title: "About"
date: 2023-09-23T18:11:41+02:00
hidemeta: true
---

Hello, I'm Peter, and I'm currently employed at a prominent German
corporation, where I serve as an IT consultant. My primary areas
of expertise revolve around DevOps and Cloud Operations,
particularly through the utilization of Infrastructure-as-Code tools.

When I'm not on the professional battleground, you'll find me
honing my skills in the serene greens of golf courses or
kicking on the soccer field. However, my true passion lies
in managing and expanding my personal network infrastructure,
both locally and in the cloud - a realm where I've traversed
exclusively through the Linux server domain.

My journey with Linux began during my bachelor's studies in
telecommunication informatics when a peer encouraged me to
adopt Linux on my personal devices. Since then, I've embraced
the open-source software (OSS) community, constantly exploring
and learning new aspects of technology. Although I'm not a
natural-born programmer, I actively engage with code, primarily
by examining and configuring existing software.

I dedicated my bachelor's thesis to the intricate realm of
Infrastructure-as-Code, specifically researching the feasibility
of providing web services through a multi-cloud approach devoid
of predefined standards at individual public cloud providers.
You can delve into my work [here][thesis], but please be aware that it's
available exclusively in German.

Committed to safeguarding privacy and data protection,
I've embarked on a master's program in cyber security.
This academic pursuit aligns seamlessly with my practice of
hosting self-managed services on my server.

[thesis]: https://d-nb.info/1287014666/34
